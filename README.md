# GAME JAM 20 10 2018

# FERRORO ROCHER

## Compilation

```bash

git clone git@gitlab.com:JAMPixel/ferrero_rocher.git
cd ferrero_rocher
sudo apt-get install cmake
sudo apt-get install libsdl2-dev
sudo apt-get install libsdl2-image-dev
sudo apt-get install libsdl2-ttf-dev
sudo apt-get install libsdl2-mixer-dev
git submodule init src/castor
git submodule update src/castor
cd src/castor
git submodule init src/SANDAL2
git submodule update src/SANDAL2
git pull src/castor
cd ../../
mkdir build
cd build
cmake ..
make
./ferrero_rocher
```
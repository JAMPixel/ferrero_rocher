#include <random>

#include "vulcano.h"

using actor::Vulcano;

Vulcano::Vulcano(Position pos):Environment("vulcano", pos, false)
{
  loadSprite();
  _time = 0;
  _sprite = 0;
  _time = _WAITING_MIN + std::rand()%_WAITING_RAND;

  setOnClickElement(_elem, click);
  setDataElement(_elem, (void *)this);
  addClickableElement(_elem, rectangleClickable(0, 0, 1, 1), 0);
}

void Vulcano::click(Element * e, int /*b*/)
{
  Vulcano * v;

  getDataElement(e, (void **) &v);

  v->toggle();
}

void Vulcano::toggle()
{
  _state = !_state;

  if(_state == CALM) {
    setSpriteAnimationElement(_elem, 0);
    _sprite = 0;
    _time = _WAITING_MIN + std::rand()%_WAITING_RAND;
  }
}

void Vulcano::act(float dt)
{

  switch(_state) {
  case CALM:
    _time -= dt;
    if(_time <= 0) {
      _time = 0;
      _state++;
    }

    break;
  case TRIGGERED:
    _time += dt;

    if(_time > 1500) {
      _time = 0;
      _sprite++;
      _state += _sprite==(_NBSPRITE  - 1);
      nextSpriteElement(_elem);
    }

    break;
  case DISASTER:
    if(_time == 0) {
      //std::cout << _around.size() << "\n";
      for(auto a : _around) {
	a->trigger();
      }
    }

    _time += dt;

    if(_time > 2000) {
      _sprite = 0;
      _time = _WAITING_MIN + std::rand()%_WAITING_RAND;
      _state = CALM;
      nextSpriteElement(_elem);
    }
    break;
  }
}

void Vulcano::effect()
{

}

void Vulcano::loadSprite()
{
  Actor::loadSprite("../images/animation_volcan.png");

  addAnimationElement(_elem, 0);

  for(int i=0; i < _NBSPRITE; i++) {
    addSpriteAnimationElement(_elem, 0,i*1200,0,1200,1200,10,i);
  }
}

#include "PNJ.h"

using actor::PNJ;

unsigned PNJ::_idCount = 0;
unsigned PNJ::NB = 0;

PNJ::PNJ(unsigned peopleId, Position * territory):
Mob("je suis un PNJ!", 1, Position(0, 0, PNJ_W, PNJ_H)), _id(_idCount++), _peopleId(peopleId), _territory(territory) {
	randomizePosition();
	loadSprite();
	Actor::addSound("../musique/shplok.aiff");
	NB++;
}

void PNJ::randomizePosition() {
	_position.x = _territory->x + rand() % (int)(_territory->w - _territory->x - _position.w);
	_position.y = _territory->y + rand() % (int)(_territory->h - _territory->y - _position.h);
}

void PNJ::act(float dt) {
	move(dt);
}

void PNJ::move(int dx, int dy) {
	_position.x += dx;
	_position.y += dy;
}

void PNJ::move(int dt)  {

	if(_distance == 0 && _time < _waitingTime) { // Waiting ...
		_time += dt;
		setWaySpriteAnimationElement(_elem, 0, 0);
	}

	if(_time >= _waitingTime)
		{
			setWaySpriteAnimationElement(_elem, 0, 1);
			_orientation = std::rand()%8; // Set the new orientation at random
			_time = 0;
			_distance = std::rand()%170 + 30; // Set the distance that it needs to walk
			_waitingTime = 1000 + (std::rand()%5000); // Set the time it will wait
		}

		switch (_orientation) {
			case N:
				setAngleElement(_elem, 0);
				break;
			case S:
				setAngleElement(_elem, 180);
				break;
			case W:
				setAngleElement(_elem, 90);
				break;
			case E:
				setAngleElement(_elem, 270);
				break;
			case NW:
				setAngleElement(_elem, 45);
				break;
			case NE:
				setAngleElement(_elem, 315);
				break;
			case SW:
				setAngleElement(_elem, 135);
				break;
			case SE:
				setAngleElement(_elem, 225);
				break;
		}

	if(_distance > 0) // Move the mob
		{
			_position.x += _speed * ((_orientation == W || _orientation == NW || _orientation == SW) - (_orientation == E || _orientation == NE || _orientation == SE));
			_position.y += _speed * ((_orientation == N || _orientation == NW || _orientation == NE) - (_orientation == S || _orientation == SE || _orientation == SW));
			--_distance;
		}

		if (_position.x < _territory->x) {
			_position.x = _territory->x;
			_orientation = 7 - _orientation;
		} else if (_position.x >= _territory->x + _territory->w - _position.h - 1) {
			_position.x = _territory->x + _territory->w - _position.h - 1;
			_orientation = 7 - _orientation;
		} else if (_position.y < _territory->y + _position.h) {
			_position.y = _territory->y + _position.h;
			_orientation = 7 - _orientation;
		} else if (_position.y >= _territory->y + _territory->h - 1) {
			_position.y = _territory->y + _territory->h - 1;
			_orientation = 7 - _orientation;
		}


}

bool PNJ::kill(const Position& p) {
  bool res = _position.collide(p);

  if (res) {
		Actor::kill();
		Actor::playSound(0,0);
		NB--;
  }

		return res;
}

void PNJ::loadSprite() {
	int tmp = _peopleId%4;
	switch (tmp) {
		case 0:
			Actor::loadSprite("../images/perso_bleu.png");
			break;
		case 1:
			Actor::loadSprite("../images/perso_jaune.png");
			break;
		case 2:
			Actor::loadSprite("../images/perso_rouge.png");
			break;
		case 3:
			Actor::loadSprite("../images/perso_vert.png");
			break;
	}

  addAnimationElement(_elem, 0);

  for(int i = 0; i < 2; i++)
    addSpriteAnimationElement(_elem, 0, 507*i, 0, 507, 700, 10, i);

  setWaySpriteAnimationElement(_elem, 0, 1);
}

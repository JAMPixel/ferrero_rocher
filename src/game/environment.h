#ifndef ENVIRONNEMENT_H
#define ENVIRONNEMENT_H

#include <gameEngine/actor/static.h>

namespace actor {

  enum EnvironmentState { CALM, TRIGGERED, DISASTER };

  class Environment : public Static
  {
  protected:
    bool _walkable;
    int  _state;

    std::vector<Environment *> _around;
  public:
    Environment(){}
    Environment(std::string name, Position pos, bool walkable)
      :Static(name,pos), _walkable(walkable), _state(CALM){}

    virtual bool isWalkable() const { return _walkable; }
    virtual bool isDevastated() const { return _state == DISASTER; }
    virtual void addNeighbor(Environment * e) { _around.push_back(e); }
    virtual void trigger() { _state = TRIGGERED; }

		virtual int getState() { return _state; }

		void move(int dx, int dy) { _position.x += dx; _position.y += dy; }

    virtual ~Environment() = default;
  };


}  // actor

#endif /* ENVIRONNEMENT_H */

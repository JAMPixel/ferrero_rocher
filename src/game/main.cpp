#include <iostream>
#include <gameEngine/game.h>
#include <gameEngine/actor/controlable/controlable.h>
#include <random>
#include <gameEngine/ihm/color.h>
#include <gameEngine/ihm/font.h>
#include <fstream>
#include <SANDAL2/SANDAL2.h>
#include <string>

#include "sea.h"
#include "forest.h"
#include "vulcano.h"
#include "People.h"
#include "meteorite.h"
#include "Plate.h"
#include "earth.h"

// #define NB_BLOCK 5
#define SIZE_BLOCK 600

void move(actor::Actor * actor, float dt)
{
  actor::Controlable * c = dynamic_cast<actor::Controlable *>(actor);

  c->move(dt);
  c->moveCamera();

}

void progressbar(Element *e, float w, float h)
{
  setDimensionElement(e,w,h);
  updateWindow();
  displayWindow();
}

void generate(Stage * stage)
{
  int w,h;
  int nb;
  std::string fname[] = {"../mat1", "../mat2", "../mat3", "../mat4"};

  // std::cout << sizeof(fname) << " " << sizeof(std::string) << "\n";

  std::ifstream file(fname[std::rand()%(sizeof(fname) / sizeof(std::string))]);

  file >> nb;

  const int NB_BLOCK = nb;

  actor::Environment * environment[NB_BLOCK][NB_BLOCK];
  actor::Controlable * c;

  getDimensionWindow(&w, &h);

  Element * pbar = createBlock(0,3*h/8.,0,h/4.,Color::green, 5,0);
  float wbar = 0, hbar = h/4.;


  setDisplayCodeWindow(5);

  c = &stage->create<actor::Controlable>("camera", 1, Position(w/2,h/2, 0,0));

  c->addActStatement(move);
  c->setSpeed(10.f);

  for(int i = 0;i < NB_BLOCK;i++) {
    for(int j = 0; j < NB_BLOCK; j++) {
      int a;
      file >> a;
      switch(a) {
      case 0:
	environment[i][j] = &stage->create<actor::Vulcano>(Position(i*SIZE_BLOCK, j*SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK));
  	break;
      case 1:
  	environment[i][j] = &stage->create<actor::Sea>(Position(i*SIZE_BLOCK, j*SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK));
  	break;
      case 2:
  	environment[i][j] = &stage->create<actor::Forest>(Position(i*SIZE_BLOCK, j*SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK));
  	break;
      case 3:
  	environment[i][j] = &stage->create<actor::Earth>(Position(i*SIZE_BLOCK, j*SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK));
  	break;
      }
      wbar = w*(i*NB_BLOCK + (float)j)/(NB_BLOCK*NB_BLOCK);
      progressbar(pbar, wbar,hbar);
    }
  }

  actor::Plate * plt = &stage->create<actor::Plate>();

  for (int i = 0; i < NB_BLOCK; i++) {
    for (int j = 0; j < NB_BLOCK; j++) {
      if(environment[i][j]->getName() == "vulcano") {
	for(int k = i-1; k <= i+1; k++) {
	  for(int l = j-1; l <= j+1; l++) {
	    if(k >= 0 && l >= 0 && k < NB_BLOCK && l < NB_BLOCK && (i!=k || j!=l)) {
	      environment[i][j]->addNeighbor(environment[k][l]);
	    }
	  }
	}
      }

      if(environment[i][j]->isWalkable()) {
	plt->add(environment[i][j]);
	plt->add(&stage->create<actor::People>(Position(i*SIZE_BLOCK, j*SIZE_BLOCK - SIZE_BLOCK,SIZE_BLOCK,SIZE_BLOCK)));
      }
    }
  }

  stage->create<actor::Meteorite>(Position(300,300, 495, 260));

  setDisplayCodeWindow(0);
  delElement(pbar);
}

int main()
{
  int w = 1920, h = 1080;
  Game game(w,h,"Ferrero_rocher");

  Mix_Music * sound= Mix_LoadMUS("../musique/music_ferrero.WAV");

  Mix_PlayMusic(sound,-1);

  std::srand(time(NULL));

  game.run(generate);

  createBlock(0,0,w,h,Color::black,0,0);

  std::string score = "Score : " + std::to_string(actor::Plate::SCORE >> 10);
  createText(w/4., h * 3/8., w/2., h / 4., 50, FREE_MONO_BOLD, score.c_str(), Color::white, SANDAL2_BLENDED, 0, 0);

  updateWindow();
  displayWindow();

  SDL_Delay(2000);

  Mix_FreeMusic(sound);
  return 0;
}

#include "sea.h"

using actor::Sea;
using actor::Environment;

Sea::Sea(Position pos):Environment("sea", pos, false)
{
  loadSprite();
}

void Sea::loadSprite()
{
  Actor::loadSprite("../images/animation_mer.png");

  addAnimationElement(_elem, 0);
  
  for(int i=0; i < _NB_SPRITES; ++i)
    addSpriteAnimationElement(_elem, 0, i*1200, 0, 1200, 1200, 10, i);

  setWaySpriteAnimationElement(_elem, 0, 1);
}


void Sea::act(float dt)
{
  
}

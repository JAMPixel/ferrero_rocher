#ifndef __PEOPLE_H__
#define __PEOPLE_H__

#include <gameEngine/actor/static.h>
#include <vector>
#include <iostream>

#include "PNJ.h"

#define POP_START 60
#define POP_RATE  50 // rapport population reelle/visible

namespace actor {
	class People: public Static {
	private:
		unsigned _id;
		unsigned _populationSize;  // taille reelle de la population
		std::vector<PNJ *> _population;  // contient les gens visibles

		static unsigned _idCount;

	public:
		People(const Position& pos = Position());

		// modifie la taille du territoire
		void expand(int rate); // entre 0 et 100
		void reduce(int rate);

		// modifie la population reelle
		void populate(int rate);
		void depopulate(int rate);

		// ajuste la population visible selon la population reelle
		void resizePopulation();
		unsigned popSize() { return _populationSize; }

		bool kill(const Position& p);
		void move(int dx, int dy);

		// affiche la valeur des attributs sur cout
		void afficher();
		void loadSprite();
	};
}

#endif

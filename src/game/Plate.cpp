#include <gameEngine/stage.h>

#include "Plate.h"

using namespace actor;

unsigned Plate::SCORE = 0;

Plate::Plate():
Moveable("Plate", 1.f, Position()) {}

void Plate::add(Environment * env) {
	if(_map.empty()) {
		_position = env->getPosition();
	} else {
		if (env->getPosition().x < _position.x) {
			_position.w += _position.x - env->getPosition().x;
			_position.x = env->getPosition().x;
		}
		if (env->getPosition().x + env->getPosition().w > _position.x + _position.w) {
			_position.w = env->getPosition().x - _position.x + env->getPosition().w;
		}

		if (env->getPosition().y < _position.y) {
			_position.h += _position.y - env->getPosition().y;
			_position.y = env->getPosition().y;
		}

		if (env->getPosition().y + env->getPosition().h > _position.y + _position.h) {
			_position.h = env->getPosition().y - _position.y + env->getPosition().h;
		}
	}

	_map.push_back(env);
}

void Plate::add(People * ppl) {
	_people.push_back(ppl);
	_people.back()->resizePopulation();
	_people.back()->populate(100);
}

unsigned Plate::environmentSize() {
	return _map.size();
}

unsigned Plate::peopleSize() {
	return _people.size();
}

Environment * Plate::getEnvironment(unsigned i) {
	return _map[i];
}

People * Plate::getPeople(unsigned i) {
	return _people[i];
}

void Plate::resizePosition() {
	if (!_map.empty()) {
		float x = _map[0]->getPosition().x;
		float y = _map[0]->getPosition().y;
		float w = _map[0]->getPosition().w;
		float h = _map[0]->getPosition().h;

		for (unsigned i = 1; i < _map.size(); ++i) {
			if (_map[i]->getPosition().x < x) {
				w += x - _map[i]->getPosition().x;
				x = _map[i]->getPosition().x;
			}

			if (_map[i]->getPosition().w > w) {
				w = _map[i]->getPosition().x;
			}

			if (_map[i]->getPosition().y < y) {
				h += y - _map[i]->getPosition().y;
				y = _map[i]->getPosition().y;
			}

			if (_map[i]->getPosition().h > _position.h) {
				h = _map[i]->getPosition().h;
			}
		}
	}
}

bool Plate::kill(const Position& p) {
	// if (_position.collide(p)) {
	static unsigned total = _people.size();
		for (unsigned i = 0; i < _people.size(); ++i) {
		  if(_people[i]->kill(p))
		    --total;
		}

		if(PNJ::NB == 0) _stage->endStage();
	// } else {
	// 	std::cout << _position.x << " " << _position.y << " " << _position.w << " " << _position.h << " | " << p.x << " " << p.y << " " << p.< << " " << p.h << std::endl;
	// }

	return !total;
}

void Plate::act(float dt) {

	SCORE += getTotalPop();
	
	for (unsigned i = 0; i < _map.size(); ++i) {
		if (_map[i]->getState() == DISASTER) {
			// std::cout << _map[i]->getPosition().x << " " << _map[i]->getPosition().y << std::endl;
			if (kill(_map[i]->getPosition())) std::cout << "PERDU !" << std::endl;
		}
	}

	static unsigned dtime = 0;
	static bool tggl = false;

	if (!dtime) {
			dtime = 200;
			for (unsigned i = 0; i < _people.size(); ++i) {
				_people[i]->populate(10);
			}

			if (tggl) {
				for (unsigned i = 0; i < _people.size(); ++i) {
					_people[i]->depopulate(10);
				}
				tggl = !tggl;
			}
	}
	--dtime;
}

unsigned Plate::getTotalPop() {
	unsigned tot = 0;

	for (unsigned i = 0; i < _people.size(); ++i) {
		tot += _people[i]->popSize();
	}

	return tot;
}

void Plate::loadSprite() {
	for (unsigned i = 0; i < _map.size(); ++i) {
		_map[i]->loadSprite();
	}

	for (unsigned i = 0; i < _people.size(); ++i) {
		_people[i]->loadSprite();
	}
}

void Plate::move(int dx, int dy) {
	_position.x += dx;
	_position.y += dy;

	for (unsigned i = 0; i < _map.size(); ++i) {
		_map[i]->move(dx, dy);
	}

	for (unsigned i = 0; i < _people.size(); ++i) {
		_people[i]->move(dx, dy);
	}
}

void Plate::afficher(std::ostream& os) {
	os << "pos: [ " << _position.x << ", " << _position.y << ", "
	<< _position.w << ", " << _position.h << " ]" << std::endl;
}

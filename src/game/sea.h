#ifndef SEA_H
#define SEA_H

#include "environment.h"

namespace actor {
  
  class Sea : public Environment
  {
  private:
    static constexpr int _NB_SPRITES = 4;
  public:
    Sea(){}
    Sea(Position pos);

    void act(float dt);
    void loadSprite();
  
    virtual ~Sea(){}
  };
}  // actor



#endif /* SEA_H */

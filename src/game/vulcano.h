#ifndef VULCANO_H
#define VULCANO_H

#include "environment.h"

namespace actor {

  class Vulcano : public Environment
  {
  private:
    int _time;
    int _sprite;

    static constexpr int _NBSPRITE = 5;
    static constexpr int _WAITING_MIN = 2000;
    static constexpr int _WAITING_RAND = 10000;
  public:
    Vulcano(){}
    Vulcano(Position pos);
    
    void act(float dt);
    void effect();
    void loadSprite();
    void toggle();
    
    static void click(Element * e, int b);
    
    virtual ~Vulcano() = default;
  };


}  // actor

#endif /* VULCANO_H */

#ifndef __PLATE_H__
#define __PLATE_H__

#include <gameEngine/actor/moveable.hpp>
#include <vector>

#include "People.h"
#include "environment.h"

namespace actor {
	class Plate: public Moveable {
	private:
		std::vector<Environment *> _map;
		std::vector<People *> _people;

	public:
		static unsigned SCORE;
		Plate();

		void add(Environment * env);
		void add(People * ppl);

		unsigned environmentSize();
		unsigned peopleSize();
		Environment * getEnvironment(unsigned i);
		People * getPeople(unsigned i);
		bool kill(const Position& p);

		unsigned getTotalPop();

		void resizePosition();

		void act(float dt = 0);
		void loadSprite();

		void move(int dx, int dy);
		void move(float dt = 0){}

		void afficher(std::ostream& os = std::cout);
	};
}

#endif

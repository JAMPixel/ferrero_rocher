#ifndef __PNJ_H__
#define __PNJ_H__

#include <gameEngine/actor/mobs/mob.h>
#include <random>

#define PNJ_W 30
#define PNJ_H 30

namespace actor {
	class PNJ: public Mob {
	private:
		unsigned _id;
		unsigned _peopleId;
		Position* _territory;

		static unsigned _idCount;

	public:
		static unsigned NB;
		PNJ(unsigned peopleId, Position * territory);

		void act(float dt = 0);

		void randomizePosition();

		void move(int dx, int dy);
		void move(int dt = 0);

		bool kill(const Position& p);

		void loadSprite();
	};
}

#endif

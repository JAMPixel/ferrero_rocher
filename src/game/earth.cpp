#include "earth.h"

using actor::Earth;

Earth::Earth(Position pos):Environment("earth", pos, true)
{
  loadSprite();
  _time = 0;
  _sprite = 0;

  Actor::addSound("../musique/feu.aiff");
}

void Earth::act(float dt)
{
  switch(_state) {
  case CALM:
    break;
  case TRIGGERED:

    if(_time == 0) {
      nextSpriteElement(_elem);
      ++_sprite;
    }

    _time += dt;

    if(_time > _TIME_BETWEEN) {
      ++_sprite;
      nextSpriteElement(_elem);
      _state += _sprite == (_NB_SPRITE - 1);
      _time = 0;
      Actor::playSound(0,2);
    }

    break;
  case DISASTER:
    
    _time += dt;

    if(_time > _TIME_BETWEEN) {
      _time = 0;
      _sprite = 0;
      _state = CALM;
      nextSpriteElement(_elem);
    }

    break;
  }
}

void Earth::effect()
{

}

void Earth::loadSprite()
{
  Actor::loadSprite("../images/animation_feu_2.png");

  addAnimationElement(_elem, 0);

  for(int i = 0; i< _NB_SPRITE; i++)
    addSpriteAnimationElement(_elem, 0, i*1200, 0, 1200, 1200, i, 0);

}

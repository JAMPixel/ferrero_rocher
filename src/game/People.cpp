#include <gameEngine/stage.h>

#include "People.h"

using actor::People;

unsigned People::_idCount = 0;

People::People(const Position& pos):
  Static("Nous sommes...!", pos), _id(_idCount++), _populationSize(POP_START) {
  //resizePopulation();
  loadSprite();
}

void People::loadSprite()
{
  if(_elem) {
    delElement(_elem);
    _elem = NULL;
  }
}

void People::expand(int rate) {
  int dw = _position.w * rate / 200;
  int dh = _position.h * rate / 200;

  _position.x -= dw;
  _position.y -= dh;
  _position.w += dw;
  _position.h += dh;
}

void People::reduce(int rate) {
  int dw = _position.w * rate / 200;
  int dh = _position.h * rate / 200;

  _position.x += dw;
  _position.y += dh;
  _position.w -= dw;
  _position.h -= dh;
}

void People::populate(int rate) {
  _populationSize += _populationSize * rate / 100;

  resizePopulation();
}

void People::depopulate(int rate) {
  _populationSize -= _populationSize * rate / 100;

  resizePopulation();
}

void People::resizePopulation() {
  unsigned targetSize = _populationSize ? 1 + _populationSize / POP_RATE : 0;

  if (_population.size() < targetSize) {
    while (_population.size() < targetSize) {
      _population.push_back(
			    &_stage->create<PNJ>(
						_id,
						&_position
						)
			    );
    }

  } else {
    while (_population.size() > targetSize) {
      _population.pop_back();
    }
  }
}

bool People::kill(const Position& p) {
	bool res = _position.collide(p);
	if (res) {
		unsigned i = 0;
		while (i < _population.size()) {
			if (_population[i]->kill(p)) {
				_population[i] = _population.back();
				_population.pop_back();
				_populationSize = _populationSize < POP_RATE ? 0 : _populationSize - POP_RATE;
			} else {
				++i;
			}
		}
	}

	return res;
}

void People::move(int dx, int dy) {
  _position.x += dx;
  _position.y += dy;

  for (unsigned i = 0; i < _population.size(); ++i) {
    _population[i]->move(dx, dy);
  }
}

void People::afficher() {
  std::cout << "x:" << _position.x << " y:" << _position.y << " w:" << _position.w <<" h:" << _position.h << std::endl;;
  std::cout << "r: " << _populationSize << std::endl;
  std::cout << "v: " << _population.size() << std::endl;
}

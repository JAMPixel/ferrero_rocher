#ifndef FOREST_H
#define FOREST_H

#include "environment.h"

namespace actor {

  class Forest : public Environment
  {
  private:
    int       _time;
    int       _sprite;

    static constexpr int _NB_SPRITE = 3;
    static constexpr int _TIME_BETWEEN = 1000;
    
  public:
    Forest(){}
    Forest(Position pos);
    
    void act(float dt);
    void effect();
    void loadSprite();
    
    virtual ~Forest(){}
  };


}  // actor

#endif /* FOREST_H */

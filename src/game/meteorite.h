#ifndef METEORITE_H
#define METEORITE_H

#include <gameEngine/actor/moveable.hpp>

namespace actor {

  class Meteorite : public Moveable
  {
  private:
    int _time;
    bool _state;
    int _width;
    int _height;
    int _sprite;

    static constexpr int _NBSPRITE = 4;

    static constexpr int _MIN_WAITING = 1000;
    static constexpr int _WAITING_RAND = 5000;
    

  public:
    Meteorite(){}
    Meteorite(Position pos);

    void act(float dt);
    void effect();
    void loadSprite();
    void explose();
    void move(float dt);
    void toggle();

    static void click(Element * e, int b);

    virtual ~Meteorite() = default;
  };


}  // actor


#endif /* METEORITE_H */

#include <random>
#include <gameEngine/stage.h>

#include "environment.h"
#include "meteorite.h"

using actor::Meteorite;

Meteorite::Meteorite(Position pos):Moveable("Meteorite", 1, pos)
{
  _time = _MIN_WAITING + std::rand()%_WAITING_RAND;
  _state = false;
  _width = pos.w;
  _height = pos.h;
  _elem = NULL;
  _sprite = 0;

  Actor::addSound("../musique/fuiiiii.aiff");
  Actor::addSound("../musique/boum.aiff");
  
}

void Meteorite::click(Element * e, int /*b*/)
{
  Meteorite * v;

  getDataElement(e, (void **) &v);

  v->toggle();
}

void Meteorite::toggle()
{
  nextSpriteElement(_elem);
  _sprite++;
}

void Meteorite::loadSprite()
{
  Actor::loadSprite("../images/animation_meteorite.png");

  addAnimationElement(_elem, 0);
  
  for(int i=0;i<3;i++) {
    addSpriteAnimationElement(_elem, 0,1250*i, 0, 1250, 950, 10, i); 
  }

  setOnClickElement(_elem, click);
  setDataElement(_elem, (void *)this);
  addClickableElement(_elem, rectangleClickable(0, 0, 1, 1), 0);
}

void Meteorite::move(float dt)
{
  _position.w *= 0.99;
  _position.h *= 0.99;

  if(_position.w < 50) {
    
    for(auto & a : _stage->actors()) {
      if(a.get() == this) continue;
      else {
	if(a.get()->getPosition().collide(_position)) {
	if(Environment * e = dynamic_cast<Environment *>(a.get())) {
	  e->trigger();
	}
	}
      }
    }

    

    nextSpriteElement(_elem);
    _sprite++;
  }
}

void Meteorite::act(float dt)
{
  if(!_state) {
    _time -= dt;

    if(_time < 0) {
      _state = true;
      loadSprite();
      setSpriteAnimationElement(_elem, 0);
      Actor::playSound(0,-1);
    }
  }
  else {
    if(!_sprite)
      move(dt);
    else if(_sprite < _NBSPRITE-1 ){
      nextSpriteElement(_elem);
      _sprite++;
    }
    else {
      Actor::playSound(1,-1);
      delElement(_elem);
      _elem = NULL;
      _state = false;
      _time = _MIN_WAITING + std::rand()%_WAITING_RAND;
      _position.w = _width;
      _position.h = _height;
      _position.x = std::rand()%(5*300);
      _position.y = std::rand()%(5*300);
      _sprite = 0;
    }
  }
}

void Meteorite::effect()
{

}

#ifndef EARTH_H
#define EARTH_H

#include "environment.h"

namespace actor {

  class Earth : public Environment
  {
  private:
    int       _time;
    int       _sprite;

    static constexpr int _NB_SPRITE = 3;
    static constexpr int _TIME_BETWEEN = 1000;
    
  public:
    Earth(){}
    Earth(Position pos);
    
    void act(float dt);
    void effect();
    void loadSprite();
    
    virtual ~Earth(){}
  };


}  // actor

#endif /* EARTH_H */
